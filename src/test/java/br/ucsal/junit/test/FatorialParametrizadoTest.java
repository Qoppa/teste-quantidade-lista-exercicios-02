package br.ucsal.junit.test;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import br.ucsal.junit.Fatorial;

@RunWith(Parameterized.class)

public class FatorialParametrizadoTest {
	@Parameters(name = "Fatorial({0})={1}")
	public static Collection<Object[]> obterDadosTeste() {
		return Arrays.asList(new Object[][] { { 0, 1 }, { 2, 2 }, { 3, 6 }, { 5, 120 } });
	}

	@Parameter
	public Integer n;
	@Parameter(1)
	public Integer fatorialEsperado;

	@Test
	public void testarFatorial() {

		Integer fatorialAtual = Fatorial.RealizarCalculoFatorial(n);

		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
