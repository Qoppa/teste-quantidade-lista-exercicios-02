package br.ucsal.junit.test;

import org.junit.Assert;
import org.junit.Test;

import br.ucsal.junit.Fatorial;

public class FatorialTest {

	@Test
	public void testarFatorial0() {
		Integer n = 0;

		Integer fatorialEsperado = 1;

		Integer fatorialAtual = Fatorial.RealizarCalculoFatorial(n);

		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial2() {
		Integer n = 2;

		Integer fatorialEsperado = 2;

		Integer fatorialAtual = Fatorial.RealizarCalculoFatorial(n);

		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial3() {
		Integer n = 3;

		Integer fatorialEsperado = 6;

		Integer fatorialAtual = Fatorial.RealizarCalculoFatorial(n);

		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial4() {
		Integer n = 4;

		Integer fatorialEsperado = 24;

		Integer fatorialAtual = Fatorial.RealizarCalculoFatorial(n);

		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial5() {
		Integer n = 5;

		Integer fatorialEsperado = 120;

		Integer fatorialAtual = Fatorial.RealizarCalculoFatorial(n);

		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial6() {
		Integer n = 6;

		Integer fatorialEsperado = 720;

		Integer fatorialAtual = Fatorial.RealizarCalculoFatorial(n);

		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial7() {
		Integer n = 7;

		Integer fatorialEsperado = 5040;

		Integer fatorialAtual = Fatorial.RealizarCalculoFatorial(n);

		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial8() {
		Integer n = 8;

		Integer fatorialEsperado = 40320;

		Integer fatorialAtual = Fatorial.RealizarCalculoFatorial(n);

		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial9() {
		Integer n = 9;

		Integer fatorialEsperado = 362880;

		Integer fatorialAtual = Fatorial.RealizarCalculoFatorial(n);

		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial10() {
		Integer n = 10;

		Integer fatorialEsperado = 3628800;

		Integer fatorialAtual = Fatorial.RealizarCalculoFatorial(n);

		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
