package br.ucsal.junit;

import java.util.Scanner;

public class Fatorial {
	public static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		obterNumero();

	}

	public static Integer ExibirFatorial(Integer n, Integer fatorial) {
		System.out.println("Fatorial de(" + n + ")= " + fatorial);
		return fatorial;

	}

	public static Integer RealizarCalculoFatorial(Integer n) {
		Integer fatorial = 1;
		for (Integer i = 1; i <= n; i++) {
			fatorial *= i;
		}
		return ExibirFatorial(n, fatorial);

	}

	public static Integer obterNumero() {
		while (true) {

			System.out.println("Informe um numero entre 0 a 100:");
			Integer numero = scanner.nextInt();
			if (numero >= 0 && numero <= 100) {
				return RealizarCalculoFatorial(numero);

			} else {
				System.out.println("Invalido");
			}

		}

	}
}
